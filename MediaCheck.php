<?php

namespace Linkus\MediaInfo;

use Linkus\MediaInfo\MediaInfo;
use Linkus\MediaInfo\MediaHash;

/**
 * Get info about file
 *
 * @author linkus <dev.linkus@gmail.com>
 */
class MediaCheck
{
    /**
     * @var array configuration
     */
    protected $cfg;

    /**
     * @var array profile
     */
    protected $param = false;

    /**
     * @var AppLink\CoreBundle\Service\Media\MediaInfo
     */
    protected $mi = false;

    /**
     * @var AppLink\CoreBundle\Service\Media\MediaHash
     */
    protected $mh = false;

    /**
     *
     * @var array info about file
     */
    protected $info = false;

    public function __construct(array $cfg)
    {
        if (!array_key_exists('profiles', $cfg)) {
            throw new \InvalidArgumentException(__METHOD__ . ': key "profiles" not found in configuration.');
        }
        foreach ($cfg['profiles'] as $profile => $values) {
            foreach (['authorizedExt', 'authorizedType', 'hash'] as $key_profile) {
                if (!array_key_exists($key_profile, $values)) {
                    $cfg['profiles'][$profile][$key_profile] = [];
                }
            }
        }
        $this->cfg = $cfg;
        $this->mi = new MediaInfo;
        $this->mh = new MediaHash;
    }

    /**
     * Load a profile
     *
     * @param string $id
     *
     * @throws \InvalidArgumentException
     */
    public function set($id)
    {
        $this->param = false;
        if (is_array($id)) {
            $this->param = $id;
        } else {
            $this->param = [];
            if (!array_key_exists($id, $this->cfg['profiles'])) {
                throw new \InvalidArgumentException(__METHOD__ . ': Undefined id "' . $id . '"');
            }
            $this->param = $this->cfg['profiles'][$id];
        }
    }

    public function getConfig()
    {
        return $this->param;
    }

    /**
     * Main function, return info about a file
     *
     * @param string $filename
     * @param array $asserts data that match file info
     *
     * @return array
     *
     * @throws \Exception file not found
     */
    public function check($filename, array $asserts = [])
    {
        if (true !== is_file($filename)) {
            throw new \Exception(__METHOD__ . ': file "' . $filename . '" not found');
        }
        if (false === $this->param) {
            throw new \Exception(__METHOD__ . ': no configuration found');
        }
        $this->info = false;

        $verbose = array_key_exists('verbose', $this->param) ? $this->param['verbose'] : 0;
        $authExt = isset($this->param['authorizedExt']) ? $this->param['authorizedExt'] : [];
        $authType = isset($this->param['authorizedType']) ? $this->param['authorizedType'] : [];

        $this->info = $this->mi->getInfo($filename, $authExt, $authType, $verbose);



        if (isset($this->param['hash'])) {
            $this->info['hash'] = $this->mh->getHash($filename, $this->param['hash'], $this->info['type']);
        }


        foreach ($asserts as $assert) {
            $err = $this->getAsserts($assert);
            if ($err) {
                $this->info['error'][] = $err;
            }
        }
        if(!array_key_exists('error', $this->info))
        {
            $this->info['error'] = false;
        }

        return $this->info;
    }

    /**
     * Check if the name of a file contain the hash
     *
     * @param string $filename
     *
     * @return boolean|string false if no hash contained in the file
     */
    public function findVerifiedHash($filename)
    {
        return $this->mh->findHashInName($filename);
    }

    /**
     * Check if an image is not corrupted
     * Return false if no error found else array
     *
     * @todo: use jpeginfo -c | pngcheck -q
     * @param string $filename
     *
     * @return boolean|array
     */
    public function isCorrupted($filename)
    {
        if (true !== file_exists($filename)) {
            throw new \Exception(__METHOD__ . ': file "' . $filename . '" not found');
        }
        $mimetype = mime_content_type($filename);
        $return_value = null;
        if ($mimetype == 'image/jpeg' or $mimetype == 'image/gif') {
            $cmd = sprintf('jpeginfo -c "%s" 2>&1', $filename);
            exec($cmd, $return_value);
            if (!preg_match('#\[OK\]$#', $return_value[0])) {
                return [0 => $return_value[0]];
            }
            return false;
        } elseif ($mimetype == 'image/png') {
            $cmd = sprintf('pngcheck -q "%s" 2>&1', $filename);
            exec($cmd, $return_value);
            if (!empty($return_value)) {
                return $return_value;
            }
            return false;
        } elseif (preg_match('#^image/#', $mimetype)) {
            $cmd = sprintf('identify "%s" 2>&1', $filename);
            exec($cmd, $return_value);
            foreach ($return_value as $line) {
                if (preg_match('#^identify: #', $line)) {
                    return $return_value;
                }
            }
            return false;
        } elseif (preg_match('#^video/#', $mimetype)) {
            $cmd = sprintf('ffmpeg -v error -i "%s" -f null - 2>&1', $filename);
            exec($cmd, $return_value);
            if (!empty($return_value)) {
                return $return_value;
            }
            return false;
        } else {
            return false;
        }
    }

    /**
     * Access MediaInfo class
     *
     * @return AppLink\CoreBundle\Service\Media\MediaInfo
     */
    public function mediaInfo()
    {
        return $this->mi;
    }

    /**
     * Accesst MediaHash class
     *
     * @return AppLink\CoreBundle\Service\Media\MediaHash
     */
    public function mediaHash()
    {
        return $this->mh;
    }

    /**
     * Check if info returned are expected
     *
     * @param array $asserts ex: md5:
     *
     * @return boolean|string string if error, else false
     */
    protected function getAsserts($assert)
    {
        $ex = explode(':', $assert);
        if (count($ex) !== 2) {
            throw new \Exception(__METHOD__ . ': assertion "' . $assert . '" not reconized');
        }
        $key = $ex[0];
        $val = $ex[1];
        if (in_array($key, ['size', 'mimetype', 'ext', 'type'])) {
            if ($this->info[$key] != $val) {
                return 'assertion error: "' . $key . '": got: "' . $this->info[$key] . '" expected: "' . $val . '" (data)';
            }
        } elseif (in_array($key, ['width', 'height', 'duration'])) {
            if ($this->info['metadata'][$key] != $val) {
                return 'assertion error: "' . $key . '": got: "' . $this->info['metadata'][$key] . '" expected: "' . $val . '" (metadata)';
            }
        } else {
            if (!array_key_exists($key, $this->info['hash'])) {
                return 'assertion error: "' . $key . '": not found (assuming hash key)';
            }
            if ($this->info['hash'][$key] != $val) {
                return 'assertion error: "' . $key . '": got: "' . $this->info['hash'][$key] . '" expected: "' . $val . '"';
            }
        }
        return false;
    }
}
