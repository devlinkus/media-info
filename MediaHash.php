<?php

namespace Linkus\MediaInfo;

/**
 * Generate hash
 *
 * hash availble are the hash returned by hash_algis() + similar + crcarchive
 *
 * @author linkus <dev.linkus@gmail.com>
 */
class MediaHash
{

    /**
     * Generate hash
     *
     * @param string $filename
     * @param array $hash
     * @param string $mediaType
     *
     * @return array
     */
    public function getHash($filename, array $hash, $mediaType = null)
    {
        $hashData = [];
        foreach ($hash as $h) {
            if (in_array($h, hash_algos())) {
                $hashData[$h] = hash_file($h, $filename);
            }
        }
        if (in_array('similar', $hash)) {
            if (is_null($mediaType) or $mediaType == 'img') {
                $hashData['similar'] = $this->getSimilar($filename);
            }
        }
        if (in_array('crcarchive', $hash)) {
            $hashData['crcarchive'] = $this->getCrcArchive($filename);
        }

        return $hashData;
    }

    /**
     * Generate similar hash
     *
     * @param string $filename
     *
     * @return string|null
     */
    public function getSimilar($filename)
    {
        $infoIMG = getimagesize($filename);

        list($width, $height) = $infoIMG;
        if ($infoIMG['mime'] == 'image/jpeg') {
            $img = @imagecreatefromjpeg($filename);
        } elseif ($infoIMG['mime'] == 'image/png') {
            $img = @imagecreatefrompng($filename);
        } elseif ($infoIMG['mime'] == 'image/gif') {
            $img = @imagecreatefromgif($filename);
        } else {
            return null;
        }
        if (!$img) {
            return null;
        }


        $new_img = imagecreatetruecolor(8, 8);
        imagecopyresampled($new_img, $img, 0, 0, 0, 0, 8, 8, $width, $height);
        imagefilter($new_img, IMG_FILTER_GRAYSCALE);
        $colors = array();
        $sum = 0;

        for ($i = 0; $i < 8; $i++) {
            for ($j = 0; $j < 8; $j++) {
                $color = imagecolorat($new_img, $i, $j) & 0xff;
                $sum += $color;
                $colors[] = $color;
            }
        }
        $avg = $sum / 64;
        $hash = '';
        $curr = '';
        $count = 0;
        foreach ($colors as $color) {
            if ($color > $avg) {
                $curr .= '1';
            } else {
                $curr .= '0';
            }
            $count++;
            if (!($count % 4)) {
                $hash .= dechex(bindec($curr));
                $curr = '';
            }
        }
        return $hash;
    }
    
    /**
     * Return hash similar to hash found in archive
     *
     * @param string $filename
     *
     * @return string
     */
    public function getCrcArchive($filename)
    {
        return strtoupper(hash_file('crc32b', $filename));
    }
    
    /**
     * Check if a hash file is contained is the name of a file
     *
     * @param string $filename
     *
     * @return boolean|string false if no hash contained in the file
     */
    public function findHashInName($filename)
    {
        $n = pathinfo($filename, PATHINFO_FILENAME);
        foreach (hash_algos() as $algo) {
            $h = hash_file($algo, $filename);
            if (preg_match('#'.$h.'#', $n)) {
                return $algo;
            }
        }
        return false;
    }
}
