<?php

namespace Linkus\MediaInfo;

/**
 * Retrieve information on file
 *
 * @link https://ffmpeg.org/ffprobe.html ffprobe Documentation
 * @author linkus <dev.linkus@gmail.com>
 */
class MediaInfo
{
    const IMAGE_TYPE = 'img';
    const VIDEO_TYPE = 'video';
    const FLASH_TYPE = 'flash';

    /**
     * @var array|null
     */
    protected $info;

    /**
     * Main function, check mimetype and retrieve information
     *
     * @param string $filename
     * @param array $authExt list of authorized extension
     * @param array $authType list of authorized type
     * @param int $verbose verbose level (0~2)
     *
     * @return type
     */
    public function getInfo($filename, array $authExt, array $authType, $verbose = 0)
    {
        $this->info = null;
        $this->getStat($filename);
        $this->info['type'] = $this->getType($this->info['mimetype']);
        $methodType = 'get' . ucfirst($this->info['type']);
        $this->info['metadata'] = $this->$methodType($filename, $verbose);
        
        if (!empty($authExt)) {
            if (!in_array(strtolower($this->info['ext']), $authExt)) {
                $list = implode(', ', $authExt);
                $msg = 'extension file: "'.$this->info['ext'].'" not in the authorized list ('.$list.')';
                $this->info['error'][] = $msg;
            }
        }
        
        if (!empty($authType)) {
            if (!in_array(strtolower($this->info['type']), $authType)) {
                $list = implode(', ', $authType);
                $msg = 'type file: "'.$this->info['type'].'" not in the authorized list ('.$list.')';
                $this->info['error'][] = $msg;
            }
        }
        
        return $this->info;
    }

    /**
     * Retrieve basic information for all type of foles
     *
     * @param string $filename
     *
     * @return array
     */
    public function getStat($filename)
    {
        $stat = stat($filename);

        $this->info['size'] = $stat['size'];
        $this->info['timeLastAccess'] = $stat['atime'];
        $this->info['timeLastMod'] = $stat['mtime'];

        $this->info['mimetype'] = mime_content_type($filename);
        $this->info['ext'] = pathinfo($filename, PATHINFO_EXTENSION);

        return $stat;
    }

    /**
     * Get type of file based on mimetype
     *
     * @param string $mimetype
     *
     * @return string
     *
     * @throws Exception
     */
    protected function getType($mimetype)
    {
        if (preg_match('#^image/#', $mimetype)) {
            return self::IMAGE_TYPE;
        }
        if (preg_match('#^video/#', $mimetype)) {
            return self::VIDEO_TYPE;
        }
        if (preg_match('#shockwave-flash#', $mimetype)) {
            return self::FLASH_TYPE;
        } else {
            return 'common';
        }
    }

    /**
     * Mock type
     *
     * @param string $filename
     * @param int $verbose
     */
    public function getCommon($filename, $verbose = 0)
    {
    }

    /**
     * Get info on image file
     *
     * @param string $filename
     * @param int $verbose
     *
     * @return array
     */
    public function getImg($filename, $verbose = 0)
    {
        $data = [];
        $dim = $this->getImageSize($filename);
        $data = [
            'width' => $dim[0],
            'height' => $dim[1]
        ];
        if ($verbose > 1) {
            $data['raw'] = $this->FFProbe($filename);
        }
        return $data;
    }

    /**
     * Get info on video file
     *
     * @param string $filename
     * @param int $verbose
     *
     * @return array
     */
    public function getVideo($filename, $verbose = 0)
    {
        $data = [];
        if ($verbose > 0) {
            $probe = $this->FFProbe($filename);
            $stream = false;
            foreach ($probe['streams'] as $k => $v) {
                if ($v['codec_type'] == 'video') {
                    $stream = $k;
                    break;
                }
            }
            $data = [
                'width' => $probe['streams'][$stream]['width'],
                'height' => $probe['streams'][$stream]['height'],
                'duration' => $probe['format']['duration'],
            ];
            if ($verbose > 1) {
                $data['raw'] = $probe;
            }
        } else {
            $dim = $this->getImageSize($filename);
            $data = [
                'width' => $dim[0],
                'height' => $dim[1]
            ];
        }
        return $data;
    }

    /**
     * Get info on shockwave file
     *
     * @param string $filename
     * @param int $verbose
     *
     * @return array
     */
    public function getFlash($filename, $verbose = 0)
    {
        $data = [];
        $dim = $this->getImageSize($filename);
        $data = [
            'width' => $dim[0],
            'height' => $dim[1]
        ];
        return $data;
    }

    /**
     * Get resolution of image/flash file
     *
     * @param string $filename
     *
     * @return array
     */
    public function getImageSize($filename)
    {
        return getimagesize($filename);
    }

    /**
     * Verbose version of get info
     *
     * @param string $filename
     *
     * @return array
     */
    public function FFProbe($filename)
    {
        $cmdr = 'ffprobe -v quiet -print_format json -show_format -show_streams "%s"';
        $cmd = sprintf($cmdr, $filename);
        $json = shell_exec($cmd);
        return json_decode($json, true);
    }
}
