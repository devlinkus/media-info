# media-info

## Configuration

Configuration:

```php
<?php
$config = [
    'profiles' => [
        'default' => [
            // list of extension authorized
            'authorizedExt' => ['jpg', 'png', 'webm', 'etc'],
            // type of files authorized (img, flash, video)
            'authorizedType' => ['img', 'video', 'flash'],
            // level verbosity (0~2)
            'verbose' => 2,
            // list of hashs to generate
            'hash' => ['md5', 'similar'],
        ],
        'another_profile' => [
            // ...
        ],
        'minimal_profile' => [],
    ]
];
```

## Usage

#### Find information on file

```php
<?php

use Linkus\MediaInfo\MediaCheck;
$mc = new MediaCheck($config);

// choose a profile
$mc->set('default');

$data = $mc->check($filename);
```

`$data` will return somting like that:

```php
<?php
array (
  'size' => 833398,
  'timeLastAccess' => 1535663793,
  'timeLastMod' => 1481863118,
  'mimetype' => 'video/webm',
  'ext' => 'webm',
  'type' => 'video',
  'metadata' => 
  array (
    'width' => 800,
    'height' => 540,
    'duration' => '4.140000',
  ),
  'hash' => 
  array (
    'md5' => 'ac885e6ea66d2ea0bb3f43ad975adaaa',
  ),
  'error' => false,
);
```

You can assert some data:

```php
<?php
$data = $mc->check($filename, [
    'width:800',
    'height:540',
    'md5:00005e6ea66d2ea0bb3f43ad975adaaa',
    'crc32:f4866b48',
]);
```
```php
<?php
array (
  'size' => 833398,
  'timeLastAccess' => 1535663793,
  'timeLastMod' => 1481863118,
  'mimetype' => 'video/webm',
  'ext' => 'webm',
  'type' => 'video',
  'metadata' => 
  array (
    'width' => 800,
    'height' => 540,
    'duration' => '4.140000',
  ),
  'hash' => 
  array (
    'md5' => 'ac885e6ea66d2ea0bb3f43ad975adaaa',
  ),
  'error' => 
  array (
    0 => 'assertion error: "md5": got: "ac885e6ea66d2ea0bb3f43ad975adaaa" expected: "00005e6ea66d2ea0bb3f43ad975adaaa"',
    1 => 'assertion error: "crc32": not found (assuming hash key)',
  ),
);
```




#### Check file integrity

```php
<?php

use Linkus\MediaInfo\MediaCheck;
$mc = new MediaCheck($config);

$err = $mc->isCorrupted($filename);
if($err){
    print_r($err);
}
```

